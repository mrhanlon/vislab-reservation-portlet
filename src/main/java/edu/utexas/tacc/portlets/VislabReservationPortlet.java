/**
 * 
 */
package edu.utexas.tacc.portlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Version;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portal.util.comparator.UserLastNameComparator;

import edu.utexas.tacc.bean.Account;
import edu.utexas.tacc.bean.Project;
import edu.utexas.tacc.bean.VislabReservation;
import edu.utexas.tacc.bean.VislabReservationCategory;
import edu.utexas.tacc.bean.VislabResource;
import edu.utexas.tacc.bean.VislabTrainingRequest;
import edu.utexas.tacc.dao.TempDao;
import edu.utexas.tacc.dao.VislabReservationCategoryDao;
import edu.utexas.tacc.dao.VislabReservationDao;
import edu.utexas.tacc.dao.VislabResourceDao;
import edu.utexas.tacc.dao.VislabTrainingRequestDao;
import edu.utexas.tacc.exceptions.DaoException;
import edu.utexas.tacc.portlet.AbstractPortlet;

/**
 * @author Matthew R Hanlon
 *
 */
public class VislabReservationPortlet extends AbstractPortlet {

    private static Handlebars hbs = new Handlebars();
    
	@Override
	protected void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		User liferayUser = null;
		Account user = null;
		try {
			liferayUser = getCurrentUser(request);
		} catch (Exception e) {
			getLogger().error(e);
		}

		
		boolean isAdmin = false;
		if (liferayUser != null) {
			request.setAttribute("authenticated", Boolean.TRUE);
			TempDao userDao = new TempDao();
			try {
				user = userDao.getUserByUsername(liferayUser.getScreenName());
				request.setAttribute("person", user);
			} catch (DaoException e) {
				getLogger().error("Error getting user Account",e);
				throw new PortletException("Error getting user Account",e);
			}
			
			try {
				isAdmin = userHasRole(request, "Administrator") || userHasRole(request, "Vislab Admin");
			} catch (Exception e) {
				getLogger().error("Error checking user role", e);
			}
		}
		request.setAttribute("isAdmin", isAdmin);

		String view = ParamUtil.getString(request, "view", "index");
		if (view.equals("reservation")) {
			doReservationView(request, response);
		} else if (view.equals("add")) {
			doAddEditView(user, request, response);
		} else if (view.equals("request-training")) {
			doRequestTrainingView(user, request, response);
		} else if (view.equals("view-training-requests")) {
			doViewTrainingRequests(user, request, response);
		} else {
			doDefaultView(user, request, response);
		}
	}
	
	private void doDefaultView(Account user, RenderRequest request, RenderResponse response) throws PortletException, IOException {
		if (user != null) {
			if (user.isVislabTrained()) {
				// get own reservations
				try {
					List<VislabReservation> ownUpcoming = new VislabReservationDao().getByOwner(user.getId(), new DateTime(), null);
					request.setAttribute("ownUpcoming", ownUpcoming);
					
					Map<Integer,String> ownReservationICalUrl = new HashMap<Integer, String>();
					for (VislabReservation own : ownUpcoming) {
						try {
							Map<String,String> params = new HashMap<String, String>();
							params.put("resource", "ical");
							params.put("id", Integer.toString(own.getId()));
							ownReservationICalUrl.put(own.getId(), getPortletURL(PortalUtil.getHttpServletRequest(request), "/vislab-reservations", false, params, PortletRequest.RESOURCE_PHASE).toString());
						} catch (Exception e) {
							getLogger().warn("Error creating iCal resource URL", e);
						}
					}
					request.setAttribute("ownReservationICalUrl", ownReservationICalUrl);
				} catch (DaoException e) {
					getLogger().warn("Error looking up users upcoming reservations", e);
				}
			} else {
				// get existing, upcoming training sessions
				// TODO allow to join session?
				try {
					List<VislabReservation> trainingSessions = new VislabReservationDao().getByCategory(4, new DateTime(), null); // training category id = 4
					request.setAttribute("trainingSessions", trainingSessions);
				} catch (DaoException e) {
					getLogger().warn("Error looking up users upcoming reservations", e);
				}
			}
		}

		try {
			Map<String,String> urlParams = new HashMap<String, String>();
			urlParams.put("resource", "ical");
			PortletURL iCalUrl = getPortletURL(PortalUtil.getHttpServletRequest(request), "/vislab-reservations", false, urlParams, PortletRequest.RESOURCE_PHASE);
			request.setAttribute("iCalUrl", iCalUrl.toString());
		} catch (Exception e) {
			getLogger().error(e);
		}
		include("/jsp/index.jsp", request, response);
	}
	
	private void doReservationView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		int reservationId = ParamUtil.getInteger(request, "id");
		if (reservationId > 0) {
			try {
				VislabReservationDao vrDao = new VislabReservationDao();
				VislabReservation vr = vrDao.getById(reservationId);
				request.setAttribute("reservation", vr);
				
				Account user = (Account) request.getAttribute("person");
				request.setAttribute("allowActions", vr.getOwner().getId() == user.getId() || userHasRole(request, "Administrator") || userHasRole(request, "Vislab Admin"));
				
				include("/jsp/reservation.jsp", request, response);
			} catch (Exception e) {
				getLogger().error(e);
				request.setAttribute("message", e.getMessage());
				include("/jsp/error.jsp", request, response);
			}
		} else {
			include("/jsp/error.jsp", request, response);
		}
	}
	
	private void doAddEditView(Account user, RenderRequest request, RenderResponse response) throws PortletException, IOException {
		boolean valid = true;
		if (user != null) {
			VislabReservationDao reservationDao = new VislabReservationDao();
			VislabReservationCategoryDao categoryDao = new VislabReservationCategoryDao();
			VislabResourceDao resourceDao = new VislabResourceDao();
			TempDao projectDao = new TempDao();
			
			VislabReservation reservation = null;
			String formJson = ParamUtil.getString(request, "formJson");
			int reservationId = ParamUtil.getInteger(request, "id");
			if (Validator.isNotNull(formJson)) {
				try {
					reservation = new ObjectMapper().readValue(formJson, VislabReservation.class);
				} catch (Exception e) {
					getLogger().error("Error reading reservation form state", e);
					throw new PortletException(e);
				}
			} else if (reservationId > 0) {
				try {
          reservation = reservationDao.getById(reservationId);
        } catch (DaoException e) {
        	getLogger().error("Error getting reservation to edit", e);
        	throw new PortletException(e);
        }
			} else {
				reservation = new VislabReservation();
			}
			request.setAttribute("reservation", reservation);
			if (reservation.getStartDate() != null && reservation.getEndDate() != null) {
				Duration d = new Duration(reservation.getStartDate(), reservation.getEndDate());
				boolean allDay = d.getStandardDays() >= 1 && reservation.getStartDate().getHourOfDay() == 0 && reservation.getEndDate().getHourOfDay() == 0;
				request.setAttribute("allDay", allDay);
				if (allDay) {
					reservation.setEndDate(reservation.getEndDate().minusMinutes(1));
				}
			}
			
			// lookup supporting data
			List<VislabResource> resources = null;
			List<VislabReservationCategory> categories = null;
			List<Project> userProjects = null;
			try {
        resources = resourceDao.list();
				categories = categoryDao.getList();
				userProjects = projectDao.getProjectsForUser(user.getId());
			} catch (DaoException e) {
				getLogger().error("Error getting form data", e);
				SessionErrors.add(request, "data-error");
				valid = false;
			}

			Collections.sort(resources, VislabResource.getNameComparator());
			Collections.sort(categories, VislabReservationCategory.getNameComparator());
			
			request.setAttribute("userProjects", userProjects);
			request.setAttribute("resources", resources);
			request.setAttribute("categories", categories);
		} else {
			// error
			SessionErrors.add(request, "access-denied");
			valid = false;
		}
		
		if (valid) {
			include("/jsp/add.jsp", request, response);
		} else {
			if (SessionErrors.contains(request, "access-denied")) {
				request.setAttribute("message", "Access denied.");
			} else if (SessionErrors.contains(request, "data-error")) {
				request.setAttribute("message", "Error loading form data.");
			} else {
				request.setAttribute("message", "Unknown error");
			}
			include("/jsp/error.jsp", request, response);
		}
	}
	
	private void doRequestTrainingView(Account user, RenderRequest request, RenderResponse response) throws PortletException, IOException {
		if (user != null) {
			try {
				List<VislabReservation> trainingSessions = new VislabReservationDao().getByCategory(4, new DateTime(), null); // training category id = 4
				request.setAttribute("trainingSessions", trainingSessions);
			} catch (DaoException e) {
				getLogger().warn("Error looking up users upcoming reservations", e);
			}

			include("/jsp/request-training.jsp", request, response);
		} else {
			request.setAttribute("message", "You must be logged in to request Vislab training.");
			include("/jsp/error.jsp", request, response);
		}
	}
	
	private void doViewTrainingRequests(Account user, RenderRequest request, RenderResponse response) throws PortletException, IOException {
		boolean allowed = false;
		try {
			allowed = user != null && (user.isStaff() || userHasRole(request, "Administrator") || userHasRole(request, "Vislab Admin"));
		} catch (Exception e) {
			getLogger().error(e);
		}
		if (allowed) {
			try {
				DateTime now = new DateTime();
	      List<VislabTrainingRequest> trainingRequests = new VislabTrainingRequestDao().list(now, now.plusMonths(3));
	      request.setAttribute("trainingRequests", trainingRequests);
				include("/jsp/training-requests.jsp", request, response);
      } catch (DaoException e) {
  			request.setAttribute("message", "Error looking up training requests.");
  			include("/jsp/error.jsp", request, response);
      }
			
		} else {
			request.setAttribute("message", "Access denied.");
			include("/jsp/error.jsp", request, response);
		}
	}

	@Override
	public void processAction(ActionRequest request, ActionResponse response) throws PortletException, IOException {
		User liferayUser = null;
		Account user = null;
		try {
			liferayUser = getCurrentUser(request);
		} catch (Exception e) {
			getLogger().error(e);
		}

		if (liferayUser != null) {
			request.setAttribute("authenticated", Boolean.TRUE);
			TempDao userDao = new TempDao();
			try {
				user = userDao.getUserByUsername(liferayUser.getScreenName());
			} catch (DaoException e) {
				getLogger().error("Error getting user Account",e);
				throw new PortletException("Error getting user Account",e);
			}
		}
		
		String action = ParamUtil.getString(request, "action");
		if (action.equals("add")) {
			processAddAction(user, request, response);
		} else if (action.equals("request-training")) {
			processRequestTrainingAction(user, request, response);
		} else if (action.equals("cancel")) {
			processCancelAction(user, request, response);
		}
	}
	
	private void processCancelAction(Account user, ActionRequest request, ActionResponse response) throws PortletException, IOException {
		int id = ParamUtil.getInteger(request, "id");
		Map<String,String> redirectParams = new HashMap<String, String>();
		if (id > 0) {
			VislabReservationDao vrDao = new VislabReservationDao();
			try {
				VislabReservation vr = vrDao.getById(id);
				if (vr != null) {
					boolean isAdmin = false;
					try {
						isAdmin = userHasRole(request, "Administrator") || userHasRole(request, "Vislab Admin");
					} catch (Exception e) {
						getLogger().error("Error checking user role", e);
					}
					if (vr.getOwner().getId() == user.getId() || isAdmin) {
						if (vrDao.cancel(vr)) {
							SessionMessages.add(request, "cancel-reservation-success");
							redirectParams.put("view", "index");
							
							// send confirmation email
							ResourceBundle bundle = ResourceBundle.getBundle("portlet");
							String from = bundle.getString("admin.from.email");
							String[] to = new String[] { vr.getOwner().getEmail() };
							String subject = bundle.getString("email.reservation.cancelled.subject");
							
							Template tmpl = hbs.compileInline(bundle.getString("email.reservation.cancelled.body.template"));
							String body = tmpl.apply(vr);
							sendEmail(from, to, subject, body);
							
						} else {
							SessionErrors.add(request, "cancel-access-denied");
							redirectParams.put("view", "reservation");
							redirectParams.put("id", Integer.toString(id));
						}
					} else {
						getLogger().warn("ACCESS DENIED for cancel reservation: " + id);
						SessionErrors.add(request, "cancel-access-denied");
						redirectParams.put("view", "reservation");
						redirectParams.put("id", Integer.toString(id));
					}
				} else {
					getLogger().warn("Cancel request for invalid ID" + id);
					redirectParams.put("view", "index");
				}
			} catch (DaoException e) {
				getLogger().error("Error cancelling reservation", e);
				SessionErrors.add(request, "cancel-error");
				redirectParams.put("view", "reservation");
				redirectParams.put("id", Integer.toString(id));
			}
		} else {
			getLogger().warn("Cancel request for invalid ID" + id);
			redirectParams.put("view", "index");
		}
		response.sendRedirect(getPortletURL(PortalUtil.getHttpServletRequest(request), redirectParams, PortletRequest.RENDER_PHASE).toString());
	}
	
	private void processRequestTrainingAction(Account user, ActionRequest request, ActionResponse response) throws PortletException, IOException {
		if (user != null) {
			DateTime preferredDate = null;
			String preferredDateStr = ParamUtil.getString(request, "preferredDate");
			String comment = ParamUtil.getString(request, "comment");
			
			if (Validator.isNull(preferredDateStr)) {
				SessionErrors.add(request, "preferred-date-error");
			} else {
				DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm");
				preferredDate = dtf.parseDateTime(preferredDateStr);
			}
			
			if (Validator.isNull(comment)) {
				SessionErrors.add(request, "comment-error");
			}
			
			if (SessionErrors.isEmpty(request)) {
				VislabTrainingRequest vrt = new VislabTrainingRequest();
				vrt.setPreferredDate(preferredDate);
				vrt.setComment(comment);
				vrt.setRequestor(user);
				try {
					VislabTrainingRequestDao vrtDao = new VislabTrainingRequestDao();
					VislabTrainingRequest saved = vrtDao.save(vrt);
					if (saved != null) {
						SessionMessages.add(request, "request-training-saved");
						Map<String,String> urlParams = new HashMap<String, String>();
						urlParams.put("view", "index");
						PortletURL url = getPortletURL(PortalUtil.getHttpServletRequest(request), urlParams, PortletRequest.RENDER_PHASE);
						response.sendRedirect(url.toString());
						
						// send email confirmations
						ResourceBundle bundle = ResourceBundle.getBundle("portlet");
						
						String from = bundle.getString("admin.from.email");
						String[] to = new String[] { vrt.getRequestor().getEmail() };
						String subject = bundle.getString("email.training.request.subject");
						Template tmpl = hbs.compileInline(bundle.getString("email.training.request.body.template"));
						String body = tmpl.apply(vrt);
						
						sendEmail(from, to, subject, body);
						
						to = new String[] { bundle.getString("vislab.staff.email") };
						subject = bundle.getString("email.training.request.staff.subject");
						tmpl = hbs.compileInline(bundle.getString("email.training.request.staff.body.template"));
						tmpl.apply(vrt);
						sendEmail(from, to, subject, body);

					} else {
						getLogger().warn("Save response was null");
						SessionErrors.add(request, "system-error");
						response.setRenderParameter("view", "request-training");
					}
				} catch (DaoException e) {
					getLogger().error("Error saving training request", e);
					SessionErrors.add(request, "system-error");
					response.setRenderParameter("view", "request-training");
				}
			} else {
				SessionErrors.add(request, "save-error");
				response.setRenderParameter("view", "request-training");
			}
		}
	}
	
	private void processAddAction(Account user, ActionRequest request, ActionResponse response) throws PortletException, IOException {
		int reservationId = ParamUtil.getInteger(request, "id", 0);
		
		String title = ParamUtil.getString(request, "title");
		if (Validator.isNull(title)) {
			SessionErrors.add(request, "title-error");
		}
		String description = ParamUtil.getString(request, "description");
		if (Validator.isNull(description)) {
			SessionErrors.add(request, "description-error");
		}
		int categoryId = ParamUtil.getInteger(request, "categoryId");
		if (categoryId == 0) {
			SessionErrors.add(request, "category-error");
		}
		
		boolean allDay = ParamUtil.getBoolean(request, "allDay");
		String startStr, endStr;
		DateTime start = null, end = null;
		if (allDay) {
			DateTimeFormatter dtf = DateTimeFormat.forPattern("M/d/y");
			startStr = ParamUtil.getString(request, "startDate");
			if (Validator.isNull(startStr)) {
				SessionErrors.add(request, "start-date-error");
			} else {
				try {
					start = dtf.parseDateTime(startStr);
					start = start.withTimeAtStartOfDay();
				} catch (Throwable t) {
					SessionErrors.add(request, "start-date-error");
					// format exception?
				}
			}
			
			endStr = ParamUtil.getString(request, "endDate");
			if (Validator.isNull(endStr)) {
				SessionErrors.add(request, "end-date-error");
			} else {
				try {
					end = dtf.parseDateTime(endStr);
					end = end.plusDays(1).withTimeAtStartOfDay();
				} catch (Throwable t) {
					SessionErrors.add(request, "end-date-error");
					// format exception?
				}
			}
		} else {
			DateTimeFormatter dtf = DateTimeFormat.forPattern("M/d/y H:m");
			startStr = ParamUtil.getString(request, "startDateTime");
			if (Validator.isNull(startStr)) {
				SessionErrors.add(request, "start-date-time-error");
			} else {
				try {
					start = dtf.parseDateTime(startStr);
				} catch (Throwable t) {
					SessionErrors.add(request, "start-date-time-error");
					// format exception?
				}
			}
			
			endStr = ParamUtil.getString(request, "endDateTime");
			if (Validator.isNull(endStr)) {
				SessionErrors.add(request, "end-date-time-error");
			} else {
				try {
					end = dtf.parseDateTime(endStr);
				} catch (Throwable t) {
					SessionErrors.add(request, "end-date-time-error");
					// format exception?
				}
			}
		}
		if (start != null) {
			start.withZoneRetainFields(DateTimeZone.forID("America/Chicago"));
		}
		if (end != null) {
			end.withZoneRetainFields(DateTimeZone.forID("America/Chicago"));
		}
		
		if (start != null && end != null && ! user.isStaff()) {
			// check during hours
			if (start.getHourOfDay() < 9 || end.getHourOfDay() > 17) {
				SessionErrors.add(request, "after-hours-error");
			}
		}
		
		int attendees = ParamUtil.getInteger(request, "totalAttendees");
		if (attendees < 1) {
			SessionErrors.add(request, "attendees-error");
		}
		
		int[] resourceId = ParamUtil.getIntegerValues(request, "resourceId");
		if (resourceId == null || resourceId.length == 0) {
			SessionErrors.add(request, "resource-error");
		}
		
		int[] projectId = ParamUtil.getIntegerValues(request, "projectId");
		if (categoryId == 5 && (projectId == null || projectId.length == 0)) {
			SessionErrors.add(request, "project-error");
		}
		
		boolean requiresStaff = ParamUtil.getBoolean(request, "_requiresStaff");

		
		VislabReservationDao vrDao = new VislabReservationDao();
		VislabReservation reservation;
		if (reservationId > 0) {
			try {
	      reservation = vrDao.getById(reservationId);
      } catch (DaoException e) {
	      getLogger().error("Error getting existing Reservation for edit", e);
	      throw new PortletException(e);
      }
		} else {
			reservation = new VislabReservation();
			reservation.setOwner(user);
		}
		reservation.setTitle(title);
		reservation.setDescription(description);
		reservation.setStartDate(start);
		reservation.setEndDate(end);
		reservation.setTotalAttendees(attendees);
		reservation.setRequiresStaff(requiresStaff);

		VislabReservationCategory category = new VislabReservationCategory();
		category.setId(categoryId);
		reservation.setCategory(category);
		
		List<VislabResource> resources = new ArrayList<VislabResource>();
		for (int id : resourceId) {
			VislabResource res = new VislabResource();
			res.setId(id);
			resources.add(res);
		}
		reservation.setResources(resources);
		
		List<Project> projects = new ArrayList<Project>();
		for (int id : projectId) {
			Project p = new Project();
			p.setId(id);
			projects.add(p);
		}
		reservation.setProjects(projects);
		
		String onBehalfOf = ParamUtil.getString(request, "onBehalfOf");		
		if (user.isStaff() && Validator.isNotNull(onBehalfOf)) {
			// check for proxy reservation
			reservation.setCreatedBy(user);
			try {
				TempDao accountDao = new TempDao();
				Account behalfOfUser = accountDao.getUserByUsername(onBehalfOf);
				if (behalfOfUser != null) {
					reservation.setOwner(behalfOfUser);
				} else {
					SessionErrors.add(request, "on-behalf-of-error");
					behalfOfUser = new Account();
					behalfOfUser.setUsername(onBehalfOf);
					reservation.setOwner(behalfOfUser);
				}
			} catch (Exception e) {
				getLogger().error("Error looking up user for proxy reservation: " + e.getMessage());
				SessionErrors.add(request, "on-behalf-of-error");
				Account behalfOfUser = new Account();
				behalfOfUser.setUsername(onBehalfOf);
				reservation.setOwner(behalfOfUser);
			}
		}
		
		// check conflicts
		if (reservation.getStartDate() != null && reservation.getEndDate() != null && reservation.getResources().size() > 0) {
			if (! ParamUtil.getBoolean(request, "overrideConflicts", false)) {
				try {
		      List<VislabReservation> conflicts = vrDao.checkConflicts(reservation);
		      if (conflicts.size() > 0) {
		      	SessionErrors.add(request, "conflicts-error");
		      	
		      	if (user.isStaff()) {
		      		SessionErrors.add(request, "override-conflicts-error");
		      	}
		      }
	      } catch (DaoException e) {
		      getLogger().error("Error checking conflicts for reservation", e);
		      SessionErrors.add(request, "system-error");
	      }
			}
		}
		
		if (SessionErrors.isEmpty(request)) {
			VislabReservation saved = null;
			try {
				saved = vrDao.save(reservation);
      } catch (DaoException e) {
	      getLogger().error("Error saving reservation", e);
      }
			
			if (saved != null) {
				SessionMessages.add(request, "reservation-saved");
				Map<String,String> urlParams = new HashMap<String, String>();
				urlParams.put("view", "reservation");
				urlParams.put("id", Integer.toString(saved.getId()));
				PortletURL url = getPortletURL(PortalUtil.getHttpServletRequest(request), urlParams, PortletRequest.RENDER_PHASE);
				response.sendRedirect(url.toString());
				
				// send email confirmations
				ResourceBundle bundle = ResourceBundle.getBundle("portlet");

				String from = bundle.getString("admin.from.email");
				String[] to = new String[] { saved.getOwner().getEmail() };
				String subject = bundle.getString("email.reservation.created.subject");
				String template = bundle.getString("email.reservation.created.body.template");
				Template tmpl = hbs.compileInline(template);
				
				Map<String,Object> valueMap = new HashMap<String, Object>();
				valueMap.put("reservation", saved);
				valueMap.put("reservationUrl", url.toString());
				String body = tmpl.apply(valueMap);
				sendEmail(from, to, subject, body);
				
				if (saved.isRequiresStaff()) {
					to = new String[] { bundle.getString("vislab.staff.email") };
					subject = bundle.getString("email.reservation.created.staff.subject");
					template = bundle.getString("email.reservation.created.staff.body.template");
					tmpl = hbs.compileInline(template);
					body = tmpl.apply(valueMap);
					sendEmail(from, to, subject, body);
				}
			} else {
				// some error occurred?
				SessionErrors.add(request, "save-error");
				SessionErrors.add(request, "system-error");
				response.setRenderParameter("view", "add");
				response.setRenderParameter("formJson", new ObjectMapper().writeValueAsString(reservation));
			}
		} else {
			SessionErrors.add(request, "save-error");
			response.setRenderParameter("view", "add");
			response.setRenderParameter("formJson", new ObjectMapper().writeValueAsString(reservation));
		}
		
	}

	@Override
	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		String resource = ParamUtil.getString(request, "resource");
		if (resource.equals("json")) {
			String endpoint = ParamUtil.getString(request, "endpoint");
			if (endpoint.equals("calendar")) {
				serveCalendarResource(request, response);
			}
		} else if (resource.equals("ical")) {
			serveICalResource(request, response);
		} else if (resource.equals("user-autocomplete")) {
			serveUserAutocompleteResource(request, response);
		} else if (resource.equals("user-projects")) {
			serveUserProjectsResource(request, response);
		} else if (resource.equals("check-conflicts")) {
			serveCheckConflictsResource(request, response);
		}
	}
	
	private void serveUserProjectsResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		String username = ParamUtil.getString(request, "username");
		Map<String, Object> json = new HashMap<String, Object>();
		if (Validator.isNotNull(username)) {
		  TempDao dao = new TempDao();
		  try {
			  Account user = dao.getUserByUsername(username);
			  if (user != null) {
				  List<Project> projects = dao.getProjectsForUser(user.getId());
				  json.put("status", "success");
				  json.put("projects", projects);
			  } else {
					json.put("status", "error");
			  	json.put("message", "User not found.");
			  }
		  } catch (DaoException e) {
				json.put("status", "error");
		  	json.put("message", "Error looking up user projects.");
		  }
		} else {
			json.put("status", "error");
			json.put("message", "Username is required.");
		}
		response.setContentType("application/json");
		new ObjectMapper().writeValue(response.getPortletOutputStream(), json);
  }

	private void serveCheckConflictsResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		boolean allDay = ParamUtil.getBoolean(request, "allDay");
		String startStr = ParamUtil.getString(request, allDay ? "startDate" : "startDateTime"),
					 endStr = ParamUtil.getString(request, allDay ? "endDate" : "endDateTime");
		int[] resourceId = ParamUtil.getIntegerValues(request, "resourceId");
		int id = ParamUtil.getInteger(request, "id");
		if (Validator.isNotNull(startStr) && Validator.isNotNull(endStr) && Validator.isNotNull(resourceId) && resourceId.length > 0) {
			DateTime start, end;
			if (allDay) {
				DateTimeFormatter dtf = DateTimeFormat.forPattern("M/d/y");
				start = dtf.parseDateTime(startStr).withTimeAtStartOfDay();
				end = dtf.parseDateTime(endStr).plusDays(1).withTimeAtStartOfDay();
			} else {
				DateTimeFormatter dtf = DateTimeFormat.forPattern("M/d/y H:m");
				start = dtf.parseDateTime(startStr);
				end = dtf.parseDateTime(endStr);
			}
			VislabReservation reservation = new VislabReservation();
			reservation.setStartDate(start.withZoneRetainFields(DateTimeZone.forID("America/Chicago")));
			reservation.setEndDate(end.withZoneRetainFields(DateTimeZone.forID("America/Chicago")));
			for (int rid : resourceId) {
				VislabResource vr = new VislabResource();
				vr.setId(rid);
				reservation.getResources().add(vr);
			}
			if (id > 0) {
				reservation.setId(id);
			}
			try {
				VislabReservationDao dao = new VislabReservationDao();
	      List<VislabReservation> conflicts = dao.checkConflicts(reservation);
	      response.setContentType("application/json");
	      new ObjectMapper().writeValue(response.getPortletOutputStream(), conflicts);
      } catch (DaoException e) {
	      getLogger().error(e);
      }
		}
	}
	
	private void serveUserAutocompleteResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		long companyId = PortalUtil.getCompanyId(request);
		String term = ParamUtil.getString(request, "term");
		List<Map<String,String>> names = new ArrayList<Map<String,String>>();
		try {
			List<User> results = UserLocalServiceUtil.search(companyId, term, WorkflowConstants.STATUS_APPROVED, new LinkedHashMap<String, Object>(), 0, 100, new UserLastNameComparator(true));
			for (User result : results) {
				Map<String,String> map = new HashMap<String, String>();
				map.put("label", result.getFullName() + " (" + result.getScreenName() + ")");
				map.put("value", result.getScreenName());
				names.add(map);
			}
		} catch (SystemException e) {
			getLogger().warn("Error getting user autocomplete results for term: " + term + "; " + e.getClass().getName() + ": " + e.getMessage());
		}
		response.setContentType("application/json");
		new ObjectMapper().writeValue(response.getPortletOutputStream(), names);
	}

	private void serveCalendarResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		String calendar = ParamUtil.getString(request, "calendar", "all");
		// TODO allow filtering to particular category
		long start = ParamUtil.getLong(request, "start") * 1000,
				 end = ParamUtil.getLong(request, "end") * 1000;
		if (calendar.equals("all")) {
			try {
				VislabReservationDao vrDao = new VislabReservationDao();
				List<VislabReservation> reservations = vrDao.getDateRange(new DateTime(start), new DateTime(end));
				List<Map<String, Object>> entries = new ArrayList<Map<String,Object>>();
				DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm z");
				for (VislabReservation vr : reservations) {
					Map<String, Object> entry = new HashMap<String, Object>();
					entry.put("id", vr.hashCode());
					Map<String,String> params = new HashMap<String, String>();
					params.put("view", "reservation");
					params.put("id", Integer.toString(vr.getId()));
					PortletURL url = getPortletURL(PortalUtil.getHttpServletRequest(request), params, PortletRequest.RENDER_PHASE);
					entry.put("url", url.toString());
					
					entry.put("start", dateFormatter.print(vr.getStartDate()));
					Duration d = new Duration(vr.getStartDate(), vr.getEndDate());
					boolean allDay = d.getStandardDays() >= 1 && vr.getStartDate().getHourOfDay() == 0 && vr.getEndDate().getHourOfDay() == 0;
					entry.put("allDay", allDay);
					if (allDay) {
						vr.setEndDate(vr.getEndDate().minusMinutes(1));
					}
					entry.put("end", dateFormatter.print(vr.getEndDate()));
					String[] classes = new String[vr.getResources().size()];
					String[] resources = new String[vr.getResources().size()];
					for (int i = 0; i < vr.getResources().size(); i++) {
						classes[i] = vr.getResources().get(i).getName().toLowerCase().replaceAll("[^a-z0-9]", "");
						resources[i] = vr.getResources().get(i).getName();
					}
					if (resources.length == 9) {
						entry.put("title", vr.getTitle() + " (Entire Vislab)");
					} else {
						entry.put("title", vr.getTitle());
					}
					entry.put("resources", resources);
					entry.put("description", vr.getDescription());
					entry.put("className", classes);
					entries.add(entry);
				}
				
				response.setContentType("application/json");
				new ObjectMapper().writeValue(response.getPortletOutputStream(), entries);
				
			} catch (DaoException e) {
				getLogger().error("Error looking up reservations", e);
			} catch (Exception e) {
				getLogger().error(e.getMessage(), e);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
		
	}
	
	private void serveICalResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		response.setContentType("text/Calendar");
		try {
			VislabReservationDao vrDao = new VislabReservationDao();
			Calendar ical = new Calendar();
			ical.getProperties().add(Version.VERSION_2_0);
			ical.getProperties().add(CalScale.GREGORIAN);
			ical.getProperties().add(new ProdId("-//TACC Vislab Calendar//iCal4j 1.0//EN"));

			int reservationId = ParamUtil.getInteger(request, "id", 0);
			if (reservationId > 0) {
				VislabReservation reservation = vrDao.getById(reservationId);
				if (reservation != null) {
					VEvent event = createIcalEvent(reservation);
					ical.getComponents().add(event);
					String filename = reservation.getTitle().replaceAll(" ", "-").replaceAll("[^a-zA-Z0-9\\-]", "").replaceAll("--+", "-");
					response.setProperty(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename + ".ics");
				}
			} else {
				DateTime dt = new DateTime();
				List<VislabReservation> reservations = vrDao.getDateRange(dt.minusWeeks(1), null);
				for (VislabReservation vr : reservations) {
					VEvent event = createIcalEvent(vr);
					ical.getComponents().add(event);
				}
			}
			CalendarOutputter co = new CalendarOutputter();
			try {
				co.output(ical, response.getPortletOutputStream());
			} catch (ValidationException e) {
				getLogger().error("Error writing iCalendar file", e);
			}
		} catch (DaoException e) {
			getLogger().error("Error creating ical events", e);
		}
	}
	
	private VEvent createIcalEvent(VislabReservation reservation) {
		net.fortuna.ical4j.model.DateTime start = new net.fortuna.ical4j.model.DateTime(reservation.getStartDate().getMillis());
		net.fortuna.ical4j.model.DateTime end = new net.fortuna.ical4j.model.DateTime(reservation.getEndDate().getMillis());
		String resourceNames;
		if (reservation.getResources().size() == 9) {
			resourceNames = "Entire Vislab";
		} else {
			String[] nameArray = new String[reservation.getResources().size()];
			for (int i = 0; i < reservation.getResources().size(); i++) {
				nameArray[i] = reservation.getResources().get(i).getName();
			}
			resourceNames = StringUtils.join(nameArray, ", ");
		}
		VEvent ve = new VEvent(start, end, reservation.getTitle() + " (" + resourceNames + ")");
		ve.getProperties().add(new Description(reservation.getDescription()));
		return ve;
	}
	
	private boolean sendEmail(String from, String[] to, String subject, String body) {
		try {
			MailMessage mailMessage = new MailMessage();
			InternetAddress[] tos = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++) {
				tos[i] = new InternetAddress(to[i]);
			}
			mailMessage.setTo(tos);
			mailMessage.setFrom(new InternetAddress(from));
			mailMessage.setBody(body);
			mailMessage.setSubject(subject);
			mailMessage.setHTMLFormat(true);
			
			
			ResourceBundle bundle = ResourceBundle.getBundle("portlet");
			Boolean debug = Boolean.parseBoolean(bundle.getString("portlet.debug"));
			if (debug) {
				getLogger().debug("Sending email: " + mailMessage.getSubject());
			} else {
				MailServiceUtil.sendEmail(mailMessage);
			}
			return true;
		} catch (Exception e) {
			getLogger().error("Error sending email: " + subject);
			getLogger().error("To: " + StringUtils.join(to, ", ") + "; " + body);
			return false;
		}
	}

}
