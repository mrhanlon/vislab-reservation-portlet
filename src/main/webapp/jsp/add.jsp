<%@ include file="/jsp/init.jsp" %>
<%@ include file="/jsp/messages.jsp" %>

<portlet:actionURL var="formUrl">
  <portlet:param name="action" value="add" />
</portlet:actionURL>

<aui:form name="vrFm" action="${formUrl}" method="post">
  <aui:input type="hidden" name="id" value="${reservation.id}" />
  <aui:layout>
    <aui:column columnWidth="60">
      <fieldset>
        <legend>Reservation info</legend>
        <aui:input name="title" type="text" cssClass="required" size="65" value="${reservation.title}" />
        <liferay-ui:error key="title-error" message="title-error" />

        <aui:select name="categoryId" cssClass="required">
          <option value="">Choose one</option>
          <c:forEach var="cat" items="${categories}">
            <c:if test="${not cat.restricted || person.staff}">
              <c:set var="selected" value="${cat.id eq reservation.category.id || cat.name eq reservation.category.name ? 'selected' : ''}" />
              <option value="${cat.id}" ${selected}>${cat.name}</option>
            </c:if>
          </c:forEach>
        </aui:select>
        <liferay-ui:error key="category-error" message="category-error" />

        <aui:input name="description" type="textarea" cssClass="description required" rows="4" cols="80" value="${reservation.description}" />
        <p class="description-length"></p>
        <liferay-ui:error key="description-error" message="description-error" />

        <div class="dates <c:if test="${allDay}">all-day-set</c:if>">
          <aui:layout>
            <aui:column first="true">
              <joda:format var="startDateVal" value="${reservation.startDate}" pattern="MM/dd/yyyy" />
              <joda:format var="startDateTimeVal" value="${reservation.startDate}" pattern="MM/dd/yyyy HH:mm" />
              <aui:input name="startDate" cssClass="required datepicker startdate" value="${startDateVal}" />
              <aui:input name="startDateTime" cssClass="required datetimepicker startdate" value="${startDateTimeVal}" />
            </aui:column>

            <aui:column>
              <joda:format var="endDateVal" value="${reservation.endDate}" pattern="MM/dd/yyyy" />
              <joda:format var="endDateTimeVal" value="${reservation.endDate}" pattern="MM/dd/yyyy HH:mm" />
              <aui:input name="endDate" cssClass="required datepicker enddate" value="${endDateVal}" />
              <aui:input name="endDateTime" cssClass="required datetimepicker enddate" value="${endDateTimeVal}" />
            </aui:column>

            <aui:column>
              <label for="<portlet:namespace/>allDay"><span><b>All day</b></span></label>
              <input type="checkbox" name="<portlet:namespace/>allDay" id="<portlet:namespace/>allDay" class="all-day" <c:if test="${allDay}">checked</c:if> />
            </aui:column>
          </aui:layout>
          <p class="timezone-info">(Enter reservation times in Central time.)</p>
          <liferay-ui:error key="start-date-error" message="start-date-error" />
          <liferay-ui:error key="start-date-time-error" message="start-date-time-error" />
          <liferay-ui:error key="end-date-error" message="end-date-error" />
          <liferay-ui:error key="end-date-time-error" message="end-date-time-error" />
          <liferay-ui:error key="after-hours-error" message="after-hours-error" />
        </div>

        <aui:input type="text" name="totalAttendees" size="10" value="${reservation.totalAttendees}" />
          <liferay-ui:error key="attendees-error" message="attendees-error" />
      </fieldset>

      <fieldset class="resources">
        <legend>Requested resources</legend>
        <liferay-ui:error key="resource-error" message="resource-error" />

        <c:forEach var="res" items="${resources}">
          <c:set var="checked" value="" />
          <c:forEach var="resres" items="${reservation.resources}">
            <c:if test="${resres.id eq res.id}">
              <c:set var="checked" value="checked" />
            </c:if>
          </c:forEach>
          <label for="<portlet:namespace/>resourceId${res.id}">
            <input type="checkbox" name="<portlet:namespace/>resourceId" id="<portlet:namespace/>resourceId${res.id}" value="${res.id}" ${checked} />
            <span>${res.name} (${res.description})</span>
          </label>
        </c:forEach>
      </fieldset>

      <fieldset class="projects">
        <legend>Projects</legend>
        <p>
          Because you have indicated that this is a Research reservation, please indicate the
          project(s) that this research is relevant to.
        </p>
        <liferay-ui:error key="project-error" message="project-error" />
        <div class="projects-wrapper">
          <c:forEach var="project" items="${userProjects}">
            <c:set var="checked" value="" />
            <c:forEach var="resproj" items="${reservation.projects}">
              <c:if test="${resproj.id eq project.id}">
                <c:set var="checked" value="checked" />
              </c:if>
            </c:forEach>
            <label for="<portlet:namespace/>projectId${project.id}">
              <input type="checkbox" name="<portlet:namespace/>projectId" id="<portlet:namespace/>projectId${project.id}" value="${project.id}" ${checked} />
              <span>${project.title} (${project.chargeCode})</span>
            </label>
          </c:forEach>
        </div>
      </fieldset>

      <fieldset>
        <legend>Requires Vislab Staff?</legend>
        <p>Please indicate if this reservation requires that Vislab staff be present.</p>
        <input type="hidden" name="<portlet:namespace/>_requiresStaff" id="<portlet:namespace/>_requiresStaff" value="${reservation.requiresStaff}" />
        <label for="<portlet:namespace/>requiresStaff">
          <input type="checkbox" name="<portlet:namespace/>requiresStaff" id="<portlet:namespace/>requiresStaff" ${reservation.requiresStaff ? 'checked' : ''}/>
          <span>Yes, require Vislab staff to be present</span>
        </label>
      </fieldset>
    </aui:column>

    <aui:column columnWidth="40">
      <c:if test="${person.staff}">
        <div class="staff-options">
          <fieldset>
            <legend>Staff options</legend>
            <c:if test="${not empty reservation.createdBy}">
              <c:set var="onBehalfOfName" value="${reservation.owner.username}" />
            </c:if>
            <aui:input name="onBehalfOf" helpMessage="on-behalf-of-help" type="text" cssClass="on-behalf-of" size="25" value="${onBehalfOfName}" tabIndex="1" />
            <liferay-ui:error key="on-behalf-of-error" message="on-behalf-of-error" />

            <div class="conflicts">
              <div class="required">
                <label for="<portlet:namespace/>overrideConflicts">
                  <input type="checkbox" name="<portlet:namespace/>overrideConflicts" id="<portlet:namespace/>overrideConflicts" tabIndex="1" />
                  <span><liferay-ui:message key="override-conflicts" /></span>
                </label>
              </div>
              <p class="disclaimer portlet-msg-alert">
                By checking this box you are acknowledging that this reservation conflicts with another reservation, but allowing the reservation anyway.
              </p>
            </div>
          </fieldset>
        </div>
      </c:if>

      <div class="conflicts">
        <p>
          This reservation conflicts with other reservations on the calendar.
          Another reservation has requested to use one or more of the resources you have selected at the same time.
          See below for details.
        </p>
        <ul class="conflicts-list"></ul>
      </div>
    </aui:column>
  </aui:layout>

  <aui:button-row>
    <aui:button type="submit" name="submit" />
    <c:choose>
      <c:when test="${reservation.id eq 0}">
        <a href='<portlet:renderURL><portlet:param name="view" value="index" /></portlet:renderURL>'>Cancel</a>
      </c:when>
      <c:otherwise>
        <a href='<portlet:renderURL><portlet:param name="view" value="reservation" /><portlet:param name="id" value="${reservation.id}" /></portlet:renderURL>'>Cancel</a>
      </c:otherwise>
    </c:choose>
  </aui:button-row>
</aui:form>

<script>
(function($) {
  $(document).ready(function() {
    $('#<portlet:namespace/>requiresStaff').bind('click', function() {
      $('#<portlet:namespace/>_requiresStaff').val($(this).is(':checked'));
    });

    $('#<portlet:namespace/>categoryId').bind('change', function() {
      if ($(this).val() === "5") { // Research
        $('.vislab-reservation-portlet .projects').slideDown();
      } else {
        $('.vislab-reservation-portlet .projects').slideUp();
      }
    }).trigger('change');

    function _formatDate(date) {
      var m = date.getMonth() + 1,
          d = date.getDate(),
          y = date.getFullYear(),
          hr = date.getHours(),
          min = date.getMinutes();
      return ('0' + m).slice(-2) + '/' + ('0' + d).slice(-2) + '/' + y + ' ' + ('0' + hr).slice(-2) + ":" + ('0' + min).slice(-2);
    }

    function checkConflicts() {
      var formData = $('#<portlet:namespace/>vrFm').serializeArray();
      var hasResourceId,hasStartDate,hasEndDate,hasStartDateTime,hasEndDateTime;
      for (var i = 0; i < formData.length; i++) {
        switch (formData[i].name) {
          case '<portlet:namespace/>resourceId': hasResourceId = true; break;
          case '<portlet:namespace/>startDate': hasStartDate = formData[i].value != ''; break;
          case '<portlet:namespace/>endDate': hasEndDate = formData[i].value != ''; break;
          case '<portlet:namespace/>startDateTime': hasStartDateTime = formData[i].value != ''; break;
          case '<portlet:namespace/>endDateTime': hasEndDateTime = formData[i].value != ''; break;
        }
      }
      if (hasResourceId && ((hasStartDate && hasEndDate) || (hasStartDateTime && hasEndDateTime))) {
        $.ajax({
          url: '<portlet:resourceURL><portlet:param name="resource" value="check-conflicts"/></portlet:resourceURL>',
          data: formData,
          success: function(resp) {
            if (resp.length > 0) {
              // conflicts!
              var list = $('.conflicts-list').empty(),
                  templ = '<span class="title">{{title}} <a href="<portlet:renderURL><portlet:param name="view" value="reservation"/></portlet:renderURL>&<portlet:namespace/>id={{id}}" target="_blank">View &rarr;</a></span><dl><dt>Start</dt><dd>{{startDate}}</dd><dt>End</dt><dd>{{endDate}}</dd><dt>Resources</dt><dd>{{#resources}}<span class="resource">{{name}}</span>{{/resources}}</dd></dl>';
              for (var i = 0; i < resp.length; i++) {
                var conf = resp[i];
                conf.startDate = _formatDate(new Date(conf.startDate));
                conf.endDate = _formatDate(new Date(conf.endDate));
                $('<li>').html($.mustache(templ, resp[i])).appendTo(list);
              }
              $('.conflicts').fadeIn();
            } else {
              // no conflicts!
              $('.conflicts').fadeOut();
            }
          }
        });
      } else {
        // cannot check conflicts -- not enough info provided
        $('.conflicts').fadeOut();
      }
    }

    var dtDefaults = {
      hour: 9,
      addSliderAccess: true,
      sliderAccessArgs: { touchonly: false },
      stepMinute: 15,
      timeFormat: 'hh:mm tt',
      minDate: (function(date) {date.setHours(9);date.setMinutes(0);date.setSeconds(0);return date;})(new Date())
    }

    var onSelectStartDate = function(dateval) {
      var d = new Date(dateval),
          e;
      if (this.id == '<portlet:namespace/>startDate') {
        $('#<portlet:namespace/>startDateTime').datepicker('setDate', d);
        e = $('#<portlet:namespace/>endDate').val();
      } else {
        $('#<portlet:namespace/>startDate').datepicker('setDate', d);
        e = $('#<portlet:namespace/>endDateTime').val();
      }

      $('#<portlet:namespace/>endDate').datepicker('option', 'minDate', d);
      $('#<portlet:namespace/>endDateTime').datepicker('refresh').datepicker('option', 'minDate', d).datetimepicker('option', 'minDateTime', d);

      e = e ? new Date(e) : d;
      if (e.compareTo(d) !== 1) {
        e = d.clone().addHours(1);
        $('#<portlet:namespace/>endDate').datepicker('setDate', e);
        $('#<portlet:namespace/>endDateTime').datepicker('setDate', e);
      }
      checkConflicts();
    }

    var onSelectEndDate = function(dateval) {
      var d = new Date(dateval);
      if (this.id == '<portlet:namespace/>endDate') {
        $('#<portlet:namespace/>endDateTime').datepicker('setDate', d);
      } else {
        $('#<portlet:namespace/>endDate').datepicker('setDate', d);
      }
      checkConflicts();
    }

    var startOpts = $.extend({}, dtDefaults, {
      onSelect: onSelectStartDate
    });

    var endOpts = $.extend({}, dtDefaults, {
      onSelect: onSelectEndDate,
      minDate: '${reservation.startDate}' ? new Date('${reservation.startDate}') : dtDefaults.minDate
    });

    $('#<portlet:namespace/>startDate').datepicker(startOpts);
    $('#<portlet:namespace/>startDateTime').datetimepicker(startOpts);

    $('#<portlet:namespace/>endDate').datepicker(endOpts);
    $('#<portlet:namespace/>endDateTime').datetimepicker(endOpts);

    $('.datepicker input,.datetimepicker input').datepicker('refresh');

    $('.all-day').bind('click', function() {
      $(this).closest('.dates').toggleClass('all-day-set');
    });

    $('#<portlet:namespace/>description').bind('input', function() {
      var rem = 255 - $(this).val().length,
          notify = $('.description-length');
      if (rem > 10) {
        notify.removeClass('warn error');
      } else {
        notify.addClass('warn');
        if (rem < 0) {
          notify.addClass('error');
        } else {
          notify.removeClass('error');
        }
      }
      notify.text(rem);
    }).trigger('input');

    function renderProjectOptions(projects) {
      var templ = '<label for="<portlet:namespace/>projectId{{id}}"><input type="checkbox" name="<portlet:namespace/>projectId" id="<portlet:namespace/>projectId{{id}}" value="{{id}}" /> <span>{{title}} ({{chargeCode}})</span></label>',
          wrapper = $('.projects-wrapper');
      wrapper.empty();
      for (var i = 0; i < projects.length; i++) {
        wrapper.append($.mustache(templ, projects[i]));
      }
    }

    $('.projects-wrapper').data('default-opts', $('.projects-wrapper').html());

    $('#<portlet:namespace/>onBehalfOf').autocomplete({
      autoFocus: true,
      delay: 150,
      source: '<portlet:resourceURL><portlet:param name="resource" value="user-autocomplete"/></portlet:resourceURL>',
      change: function(e, ui) {
        var self = $(this);
        var username = self.val();
        if (username) {
          // look up user projects
          var projects = self.data(username+'-projects');
          if (projects) {
            renderProjectOptions(projects);
          } else {
            $.ajax({
              url: '<portlet:resourceURL><portlet:param name="resource" value="user-projects" /></portlet:resourceURL>',
              data: '<portlet:namespace/>username=' + username,
              success: function(resp) {
                if (resp.status == 'success') {
                  self.data(username+'-projects', resp.projects);
                  renderProjectOptions(resp.projects);
                } else {
                  alert("Could not find projects for user: " + username);
                }
              },
              error: function() {
                alert("Could not find projects for user: " + username);
              }
            });
          }
        } else {
          $('.projects-wrapper').html($('.projects-wrapper').data('default-opts'));
        }
      }
    });

    $('input[name=<portlet:namespace/>resourceId]').bind('click', checkConflicts);

    // check right away in case of form re-render
    checkConflicts();
  });
})(jQuery);
</script>
