<%@ include file="/jsp/init.jsp" %>

<liferay-ui:success key="cancel-reservation-success" message="cancel-reservation-success" />
<liferay-ui:success key="request-training-saved" message="request-training-saved" />
<liferay-ui:success key="reservation-saved" message="reservation-saved" />

<liferay-ui:error key="save-error" message="save-error" />
<liferay-ui:error key="system-error" message="system-error" />
<liferay-ui:error key="cancel-access-denied" message="cancel-access-denied" />
<liferay-ui:error key="cancel-error" message="cancel-error" />
<liferay-ui:error key="conflicts-error" message="conflicts-error" />
<liferay-ui:error key="override-conflicts-error" message="override-conflicts-error" />