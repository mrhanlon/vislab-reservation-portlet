<%@ include file="/jsp/init.jsp" %>
<%@ include file="/jsp/messages.jsp" %>

<portlet:actionURL var="formUrl">
  <portlet:param name="action" value="request-training" />
</portlet:actionURL>

<p>
	Because the Vislab equipment has changed significantly, TACC requires all users, including previous Vislab users, to be trained on the new equipment prior to making reservations. We regret any inconvenience. 
</p>
<p>
	If you have any questions, please <a href="/group/tup/tacc-consulting?t_action=create">open a consulting ticket</a>.
</p>

<aui:form name="vrFm" action="${formUrl}" method="post">
  <c:if test="${not empty trainingSessions}">
    <fieldset>
      <legend>Upcoming Vislab Training Sessions</legend>
      <p>The following training sessions are currently scheduled.</p>
      <ul class="reservation-list">
        <c:forEach var="ts" items="${trainingSessions}">
          <li>
            <div><span class="title">${ts.title}</span></div>
            <span class="date">
              <a class="ical" href='<portlet:resourceURL><portlet:param name="resource" value="ical"/><portlet:param name="id" value="${ts.id}"/></portlet:resourceURL>' title="Download to calendar">
                <joda:format value="${ts.startDate}" style="SM" /> - <joda:format value="${ts.endDate}" style="-M" />
              </a>
            </span>
          </li>
        </c:forEach>
      </ul>
    </fieldset>
  </c:if>

	<fieldset>
		<legend>Request a Vislab Training Session</legend>
		<p>
			<c:if test="${not empty trainingSessions}">If none of the above times will work for you, you may request a different date.</c:if>
			Please indicate your preferred date and time to do your Vislab training.  TACC Vislab Staff will schedule training sessions based on need and scheduling requests.
		</p>
		<aui:input type="text" name="preferredDate" cssClass="required datetimepicker" size="45" value="${training.preferredDate}" />
		<liferay-ui:error key="preferred-date-error" message="preferred-date-error" />
    <p class="timezone-info">(Enter times in Central time.)</p>
		<aui:input type="textarea" name="comment" cssClass="comment required" rows="5" cols="80" value="${training.comment}"/>
    <p class="comment-length"></p>
    <liferay-ui:error key="comment-error" message="comment-error" />
	</fieldset>
  <aui:button-row>
    <aui:button type="submit" name="submit" />

    <portlet:renderURL var="cancelUrl">
      <portlet:param name="view" value="index" />
    </portlet:renderURL>
    <a href="${cancelUrl}">Cancel</a>
  </aui:button-row>  

</aui:form>
<script>
(function($) {
  $(document).ready(function() {
    var dtpOpts = {
      hour: 9,
      addSliderAccess: true,
      sliderAccessArgs: { touchonly: false },
      minDate: (function(date) {date.setHours(9);date.setMinutes(0);date.setSeconds(0);return date;})(new Date()),
      stepMinute: 15
    }
		$('.datetimepicker input').datetimepicker(dtpOpts);

    $('#<portlet:namespace/>comment').bind('input', function() {
      var rem = 255 - $(this).val().length,
          notify = $('.comment-length');
      if (rem > 10) {
        notify.removeClass('warn error');
      } else {
        notify.addClass('warn');
        if (rem < 0) {
          notify.addClass('error');
        } else {
          notify.removeClass('error');
        }
      }
      notify.text(rem);
    }).trigger('input');
	});
})(jQuery);
</script>