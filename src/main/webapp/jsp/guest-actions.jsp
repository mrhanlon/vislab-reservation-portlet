<h2>TACC Vislab</h2>

<%@ include file="/jsp/subscribe.jsp" %>

<hr>

<h2>Are you ready to use the TACC Vislab?</h2>
<p>
  TACC's Vislab Reservation System gives you the ability to schedule use
  of Vislab resources ahead of time, not only ensuring those resources
  will be available for you when you need them, but also allowing you to
  request assistance from Vislab staff if needed and coordinate
  reservations with other attendees.
</p>

<portlet:renderURL var="currentUrl">
  <portlet:param name="view" value="index" />
</portlet:renderURL>
<p>
  If you have a TACC account, please <b><a href="/c/portal/login?redirect=${currentUrl}">log in</a></b> to schedule a Vislab
  training session, view reservation details, and manage your Vislab
  reservations.
</p>

<p>
  If you are a new TACC user, you must first <b><a href="/account-request">request a TACC account</a></b>.
</p>