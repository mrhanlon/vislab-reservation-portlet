<%@ include file="/jsp/init.jsp" %>
<%@ include file="/jsp/messages.jsp" %>

<portlet:renderURL var="calendarUrl">
  <portlet:param name="view" value="index" />
</portlet:renderURL>
<a href="${calendarUrl}" class="back-link">Back to calendar</a>
<hr>
<div class="reservation">
  <h2>${reservation.title}</h2>

  <p class="description">${reservation.description}</p>

  <dl>
    <dt>Created by<dt>
    <dd>${reservation.owner.firstName} ${reservation.owner.lastName}</dd>

    <dt>Reservation date</dt>
    <dd><joda:format value="${reservation.startDate}" style="MM" /> - <joda:format value="${reservation.endDate}" style="MM" /></dd>

    <dt>Attendees</dt>
    <dd>${reservation.totalAttendees}</dd>

    <dt>Category</dt>
    <dd>${reservation.category.name}</dd>

    <dt>Reserved resources</dt>
    <dd>
      <ul class="resources">
        <c:forEach var="resource" items="${reservation.resources}">
          <li>${resource.name}</li>
        </c:forEach>
      </ul>
    </dd>

    <dt>Download</dt>
    <dd>
        <portlet:resourceURL var="iCalUrl">
          <portlet:param name="resource" value="ical"/>
          <portlet:param name="id" value="${reservation.id}"/>
        </portlet:resourceURL>
        <a class="ical" href="${iCalUrl}">Download this to your calendar</a>
    </dd>
  </dl>

  <c:if test="${allowActions}">
    <hr>
    <h3>Actions</h3>
    <ul>
      <li>
        <portlet:renderURL var="editUrl">
          <portlet:param name="view" value="add" />
          <portlet:param name="id" value="${reservation.id}" />
        </portlet:renderURL>
        <a href="${editUrl}" class="edit-link">Edit this reservation</a>
      </li>
      <li>
        <portlet:actionURL var="cancelUrl">
          <portlet:param name="action" value="cancel" />
          <portlet:param name="id" value="${reservation.id}" />
        </portlet:actionURL>
        <a href="${cancelUrl}" class="cancel-link">Cancel this reservation</a>
      </li>
    </ul>
  </c:if>
</div>
<script type="text/javascript">
(function ($) {
$(document).ready(function() {
  $('.cancel-link').bind('click', function() {
    if (! confirm("Are you sure you want to cancel this reservation?")) {
      return false;
    }
  });
});
})(jQuery);
</script>
