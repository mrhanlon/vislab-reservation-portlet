<%@ include file="/jsp/init.jsp" %>
<%@ include file="/jsp/messages.jsp" %>

<aui:layout>
  <aui:column columnWidth="25" first="true">
    <c:choose>
      <c:when test="${authenticated}">
        <%@ include file="/jsp/authenticated-actions.jsp" %>
      </c:when>

      <c:otherwise>
        <%@ include file="/jsp/guest-actions.jsp" %>
      </c:otherwise>
    </c:choose>
  </aui:column>
  <aui:column columnWidth="75" last="true">
    <%@ include file="/jsp/calendar.jsp" %>
  </aui:column>
</aui:layout>