<%@ include file="/jsp/init.jsp" %>

<div id="<portlet:namespace/>calendar"></div>
<liferay-portlet:resourceURL var="calendarJsonUrl" copyCurrentRenderParameters="false">
  <portlet:param name="resource" value="json" />
  <portlet:param name="endpoint" value="calendar" />
  <portlet:param name="calendar" value="all" />
</liferay-portlet:resourceURL>
<script>
(function($){
	$(document).ready(function() {
	  var tooltipShown = false,
			  portlet = $('#p_p_id<portlet:namespace/>'),
			  getTooltipPosition = function(e, tooltip) {
			    var tl = tooltip.offset().left,
			        tw = tooltip.width(),
			        pw = portlet.width(),
			        portletOffset = portlet.offset(),
			        rightBound = portletOffset.left + pw;

			    var left = tooltip.data('left') || tl + tw + 25 > rightBound;
			    if (left) {
			      tooltip.data('left', true);
			      return {
			        top: e.pageY - portletOffset.top + 20,
			        left: '',
			        right: pw - e.pageX + portletOffset.left + 20
			      }
			    } else {
			      return {
			        top: e.pageY - portletOffset.top + 20,
			        left: e.pageX - portletOffset.left + 20,
			        right: ''
			      }
			    }
			  };
		$("#<portlet:namespace/>calendar").fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			eventRender: function(event, element) {
				// double html escapes
				$('.fc-event', element).html(event.title)
				  .addClass(event.className.join(" "));

				// get tooltip
				var tooltip = $('.calendar-tooltip-' + event.id);
				if (tooltip.length == 0) {
				  // create tooltip
				  var templ = '<div class="calendar-tooltip calendar-tooltip-{{id}}"><div class="title">{{title}}</div><div class="description">{{description}}</div><dl><dt>Start</dt><dd>{{start}}</dd><dt>End</dt><dd>{{end}}</dd><dt>Resources</dt><dd><ul>{{#resources}}<li>{{.}}</li>{{/resources}}</ul></dd></dl></div>',
				  		html = $.mustache(templ, event);
				  tooltip = $(html);
          portlet.append(tooltip);
        }

				$(element).bind('mouseenter', function(e) {
            tooltipShown = true;
				    var self = $(this),
				        t = self.data('timeout');
				    if (t) {
				      clearTimeout(t);
				    }
				    t = setTimeout(function() {
              tooltip.css(getTooltipPosition(e, tooltip)).fadeIn(100);
            }, 100);
            self.data('timeout',t);
				  }).bind('mouseleave', function(e) {
            tooltipShown = false;
				    var self = $(this),
				        t = self.data('timeout');
				    if (t) {
				      clearTimeout(t);
				    }
				    t = setTimeout(function() {
              tooltip.hide();
            }, 100);
            self.data('timeout',t);
				  }).bind('mousemove', function(e) {
				    if (tooltipShown) {
              tooltip.css(getTooltipPosition(e, tooltip));
				    }
				  });
			},
			editable: false,
			events: '${calendarJsonUrl}',
			allDayDefault: false,
			defaultView: 'agendaWeek',
			firstHour: 8,
			weekMode: 'liquid',
			height: 600
		});
	});
})(jQuery);
</script>