<%@ include file="/jsp/init.jsp" %>

<div class="error">
  <h2>Error</h2>
  
  <p>
    <c:choose>
      <c:when test="${not empty message}">
        ${message}
      </c:when>
      <c:otherwise>
        An unexpected error occurred.
      </c:otherwise>
    </c:choose>
  </p>
  
  <p>
    If this problem persists, please contact the Help Desk.
  </p>
</div>