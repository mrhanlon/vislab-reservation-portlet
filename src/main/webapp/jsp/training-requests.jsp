<%@ include file="/jsp/init.jsp" %>
<%@ include file="/jsp/messages.jsp" %>

<portlet:renderURL var="calendarUrl">
  <portlet:param name="view" value="index" />
</portlet:renderURL>
<a class="back-link" href="${calendarUrl}">Back to calendar</a>

<h2>User requested training dates</h2>
<p>
	Listed below are the user-requested training dates for the next three months.
	
	<portlet:renderURL var="createTrainingUrl">
		<portlet:param name="view" value="add" />
	</portlet:renderURL>
	<a href="${createTrainingUrl}">Schedule a training session.</a>
</p>

<table class="training-request-table">
	<thead>
		<tr>
			<th>Preferred training time</th>
			<th>Requesting user</th>
			<th>Comments</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="tr" items="${trainingRequests}">
			<tr>
				<td><joda:format value="${tr.preferredDate}" style="SM" /></td>
				<td>${tr.requestor.firstName} ${tr.requestor.lastName} (<a href="mailto:${tr.requestor.email}">${tr.requestor.email}</a>)</td>
				<td>${tr.comment}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script>
	(function($) {
		$(document).ready(function() {
			$('table.training-request-table').dataTable();
		});
	})(jQuery);
</script>