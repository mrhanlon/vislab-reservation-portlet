<%@ include file="/jsp/init.jsp" %>

<div class="actions">
  <c:choose>
    <c:when test="${person.vislabTrained}">
      <h2>TACC Vislab</h2>

      <portlet:renderURL var="addUrl">
        <portlet:param name="view" value="add" />
      </portlet:renderURL>
      <h4><a class="create" href="${addUrl}">Create a new Vislab reservation</a></h4>

      <%@ include file="/jsp/subscribe.jsp" %>

      <hr>

      <h3>Your Vislab Reservations</h3>
      <c:if test="${empty ownUpcoming}">
        <p>You have no upcoming Vislab reservations.</p>
      </c:if>
      <c:if test="${not empty ownUpcoming}">
        <ul class="reservation-list">
          <c:forEach var="res" items="${ownUpcoming}">
            <li>
              <portlet:renderURL var="reservationUrl">
                <portlet:param name="view" value="reservation" />
                <portlet:param name="id" value="${res.id}" />
              </portlet:renderURL>
              <div class="title">${res.title}</div>
              <div class="date"><joda:format value="${res.startDate}" pattern="MM/dd/yyyy HH:mm" /></div>
              <a href="${ownReservationICalUrl[res.id]}" class="ical" title="Download to calendar">Download to calendar</a>
              <a href="${reservationUrl}">View</a>
            </li>
          </c:forEach>
        </ul>
      </c:if>
    </c:when>
    <c:otherwise>
      <h2>TACC Vislab</h2>      
      <%@ include file="/jsp/subscribe.jsp" %>

      <hr>

      <h3>Vislab training</h3>
      <p>
        Before you can use the TACC Vislab we ask that you complete a Vislab training.
      </p>
      
      <portlet:renderURL var="requestTrainingUrl">
        <portlet:param name="view" value="request-training" />
      </portlet:renderURL>
      <a href="${requestTrainingUrl}">Request a vislab training session</a>

      <hr>

      <h4>Upcoming training classes</h4>
      <c:if test="${empty trainingSessions}">
        <p>There are no upcoming training sessions scheduled.</p>
      </c:if>
      <c:if test="${not empty trainingSessions}">
        <p>The following training sessions are currently scheduled.</p>
        <ul class="reservation-list">
          <c:forEach var="ts" items="${trainingSessions}">
            <li>
              <div class="title">${ts.title}</div>
              <div class="date"><joda:format value="${ts.startDate}" style="SM" /> - <joda:format value="${ts.endDate}" style="-M" /></div>
              <a class="ical" href='<portlet:resourceURL><portlet:param name="resource" value="ical"/><portlet:param name="id" value="${ts.id}"/></portlet:resourceURL>' title="Download to calendar">Download to calendar</a>
              </span>
            </li>
          </c:forEach>
        </ul>
      </c:if>
    </c:otherwise>
  </c:choose>

  <c:if test="${person.staff}">
    <hr>

    <h3>Staff</h3>

    <portlet:renderURL var="trainingUrl">
      <portlet:param name="view" value="view-training-requests" />
    </portlet:renderURL>
    <h4><a href="${trainingUrl}">View training requests</a></h4>
  </c:if>
</div>