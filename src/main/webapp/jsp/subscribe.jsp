<%@ include file="/jsp/init.jsp" %>

<a href="${iCalUrl}" class="ical popup">Subscribe to this calendar</a>
<div class="subscribe-dialog">
<p>
	You can subscribe to the TACC Vislab Calendar in your calendar application,
	such as Apple iCal, Microsoft Outlook or Google Calendar!  
	Simply copy the URL below and add it to your calendar.
</p>

<aui:field-wrapper>
	<aui:input name="ical" value="${iCalUrl}" size="70"/>
</aui:field-wrapper>

<p>
	Need help?
	<ul>
		<li><a target="_blank" href="http://docs.info.apple.com/article.html?path=iCal/4.0/en/9869.html">How to subscribe to iCalendar feeds in Apple iCal</a></li>
		<li><a target="_blank" href="http://office.microsoft.com/en-us/outlook-help/view-and-subscribe-to-internet-calendars-HA102534443.aspx">How to subscribe to iCalendar feeds in Microsoft Outlook</a></li>
		<li><a target="_blank" href="http://www.google.com/support/calendar/bin/answer.py?answer=37100">How to subscribe to iCalendar feeds in Google Calendar</a></li>
	</ul>
</p>
<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$('.popup').bind('click', function(e) {
				e.preventDefault();

				$('.subscribe-dialog').dialog({
					resizable: false,
					draggable: false,
					modal: true,
					title: "Subscribe to the TACC Vislab Calendar",
					width: 480
				});

				return false;
			});

			$('#<portlet:namespace/>ical').bind('focus', function() {
				$(this).select();
			}).bind('mouseup', function(e) {
				e.preventDefault();
			});
		});
	})(jQuery);
</script>
</div>