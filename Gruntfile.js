'use strict';

module.exports = function(grunt) {
  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    clean: {
      all: [
        '.tmp/*'
        , 'src/main/webapp/css/main.css'
      ]
    }

    , compass: {
      options: {
        sassDir: 'src/main/webapp/sass',
        cssDir: '.tmp/css',
        imagesDir: 'src/main/webapp/images',
        javascriptsDir: 'src/main/webapp/js',
        fontsDir: 'src/main/webapp/fonts',
        // importPath: 'bower_components',
        relativeAssets: true
      },
      dist: {},
      dev: {
        options: {
          debugInfo: true
        }
      }
    }

    , cssmin: {
      dist: {
        files: {
          'src/main/webapp/css/main.css': [
            '.tmp/css/main.css'
          ]
        }
      }
    }

  });

  grunt.registerTask('default', [
    'clean'
    , 'compass:dist'
    , 'cssmin'
  ]);
};
