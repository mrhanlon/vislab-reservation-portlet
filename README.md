#Vislab Reservations Portlet
##TACC User Portal

This is a Liferay portlet for managing TACC Vislab reservations.

###Requirements

The following are required build dependencies:

- grunt
- bower
- sass/compass
- maven

Then, execute `mvn package` to build the project. This will automatically execute
`npm install`, `bower install`, and run the default grunt tasks
